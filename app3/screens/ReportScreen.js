import React, { Component } from "react";
import { View, Text } from "react-native";
import {
  Container,
  Header,
  Left,
  Button,
  Icon,
  Body,
  Title,
  Right,
  Content
} from "native-base";
import ReportForm from "../component/SimpleForm";

class ReportScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <Container>
        <Header>
          <Left style={{ flex: 1 }}>
            <Button transparent>
              <Icon name="menu" />
            </Button>
          </Left>
          <Body
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
          >
            <Title>Send Report</Title>
          </Body>
          <Right />
        </Header>
        <Content>
          <ReportForm />
        </Content>
      </Container>
    );
  }
}

export default ReportScreen;
