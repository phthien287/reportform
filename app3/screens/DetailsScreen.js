import React, { Component } from "react";
import {
    Container,
    Header,
    Left,
    Button,
    Icon,
    Body,
    Content,
    Right,
    Title,
    Card,
    CardItem,
    Text,
    StyleProvider
} from "native-base";
import { Image } from "react-native";

class DetailsScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        const { navigation } = this.props;
        const item = navigation.state.params;
        console.log(item.image);
        return (
            <Container>
                <Header>
                    <Left style={{ flex: 1 }}>
                        <Button transparent onPress={() => navigation.goBack()}>
                            <Icon name="arrow-back" />
                        </Button>
                    </Left>
                    <Body
                        style={{
                            flex: 1,
                            justifyContent: "center",
                            alignItems: "center"
                        }}
                    >
                        <Title>Details</Title>
                    </Body>
                    <Right />
                </Header>
                <Content>
                    <Card>
                        <CardItem>
                            <Text>Name : {item.name}</Text>
                        </CardItem>
                        <CardItem>
                            <Text>Phone : {item.phone}</Text>
                        </CardItem>
                        <CardItem>
                            <Text>Address : {item.address}</Text>
                        </CardItem>
                        <CardItem>
                            <Text>Date : {item.date.toString()}</Text>
                        </CardItem>
                        <CardItem>
                            <Text>Content : {item.content}</Text>
                        </CardItem>
                        <CardItem>
                            <Image
                                source={{ uri: item.image }}
                                style={{ height: 200, width: 200, flex: 1 }}
                            />
                        </CardItem>
                    </Card>
                </Content>
            </Container>
        );
    }
}

export default DetailsScreen;
