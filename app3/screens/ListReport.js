import React, { Component } from "react";
import {
  Container,
  Header,
  Left,
  Button,
  Icon,
  Body,
  Content,
  Right,
  Title,
  Card,
  CardItem,
  Text,
  StyleProvider
} from "native-base";
import { TouchableOpacity, ActivityIndicator } from "react-native";
import faker from "faker";
import getTheme from "../node_modules/native-base/src/theme/components";
import commonColor from "../node_modules/native-base/src/theme/variables/commonColor";
import { isLoading } from "expo-font";

class ListReportScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      isLoading: true
    };
  }

  _loadData = () => {
    this.setState({ isLoading: true });
    var data = this.state.list;
    for (i = 0; i < 20; i++) {
      data = [
        ...data,
        {
          content: faker.lorem.paragraph(),
          address: faker.address.streetAddress(),
          phone: faker.phone.phoneNumber(),
          name: faker.name.findName(),
          date: faker.date.recent(),
          image: faker.image.image()
        }
      ];
    }
    this.setState({ list: data });
  };

  async componentWillMount() {
    await this._loadData();
    this.setState({ isLoading: false });
  }

  async _pressLoad() {
    await this._loadData();
    this.setState({ isLoading: false });
  }

  _handleOnPress = item => {
    this.props.navigation.navigate("Details", item);
    // console.log(item);
  };

  render() {
    const { list } = this.state;
    return (
      <StyleProvider style={getTheme(commonColor)}>
        <Container>
          <Header>
            <Left style={{ flex: 1 }}>
              <Button transparent>
                <Icon name="menu" />
              </Button>
            </Left>
            <Body
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Title>Report List</Title>
            </Body>
            <Right />
          </Header>
          <Content>
            {list.map((item, index) => {
              return (
                <Card key={index}>
                  <TouchableOpacity onPress={() => this._handleOnPress(item)}>
                    <CardItem header>
                      <Text>{item.name}</Text>
                    </CardItem>
                    <CardItem>
                      <Text>{item.address}</Text>
                    </CardItem>
                  </TouchableOpacity>
                </Card>
              );
            })}
            {this.state.isLoading ? (
              <ActivityIndicator />
            ) : (
              <Button transparent onPress={() => this._pressLoad()}>
                <Text>Load More -></Text>
              </Button>
            )}
          </Content>
        </Container>
      </StyleProvider>
    );
  }
}

// const styles = StyleSheet.create({
//   headerText: {
//     color: "white"
//   }
// });

export default ListReportScreen;
