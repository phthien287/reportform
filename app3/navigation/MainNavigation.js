import React, { Component } from "react";
import { createStackNavigator, createAppContainer } from "react-navigation";
import ListReportScreen from "../screens/ListReport";
import DetailsScreen from "../screens/DetailsScreen";
import ReportScreen from "../screens/ReportScreen";

const MainNavigation = createStackNavigator(
  {
    Report: {
      screen: ReportScreen,
      navigationOptions: {
        header: null
      }
    },

    List: {
      screen: ListReportScreen,
      navigationOptions: {
        header: null
      }
    },
    Details: {
      screen: DetailsScreen,
      navigationOptions: {
        header: null
      }
    }
  },
  {
    initialRouteName: "Report"
  }
);

const AppContainer = createAppContainer(MainNavigation);

export default AppContainer;
