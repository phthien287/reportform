import React from "react";
import { AppLoading, Font } from "expo";
import AppContainer from "./navigation/MainNavigation";
import { createStore } from "redux";
import allReducers from "./reducers/MainReducer";
import { Provider } from "react-redux";

const store = createStore(allReducers);
export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoadingComplete: true
    };
  }
  async componentWillMount() {
    await Font.loadAsync({
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
      Roboto: require("native-base/Fonts/Roboto.ttf")
    });
    this.setState({ isLoadingComplete: false });
  }

  render() {
    if (this.state.isLoadingComplete) {
      return <AppLoading />;
    } else {
      return (
        <Provider store={store}>
          <AppContainer />
        </Provider>
      );
    }
  }
}
