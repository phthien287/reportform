import React, { Component } from "react";
import { View, DatePickerAndroid } from "react-native";
import { ImagePicker } from "expo";
import {
  Container,
  Header,
  Body,
  Item,
  Input,
  Text,
  Label,
  Button,
  Icon
} from "native-base";
import { Field, reduxForm } from "redux-form";
import moment from "moment";

class SimpleForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedDate: new Date(),
      fileUpload: null
    };
    this._renderInput = this._renderInput.bind(this);
    this._renderDatePicker = this._renderDatePicker.bind(this);
    this._renderImagePicker = this._renderImagePicker.bind(this);
  }
  _renderInput({ input, label, type, meta: { touched, error, warning } }) {
    var hasError = false;
    if (error !== undefined) {
      hasError = true;
    }
    return (
      <Item error={hasError}>
        <Text>{label}</Text>
        <Input
          {...input}
          style={{
            borderColor: "steelblue",
            borderWidth: 1,
            margin: 5,
            height: 37,
            width: 220,
            padding: 5
          }}
        />
        {hasError ? <Text>{error}</Text> : <Text />}
      </Item>
    );
  }
  _renderContent({ input, label, type, meta: { touched, error, warning } }) {
    var hasError = false;
    if (error !== undefined) {
      hasError = true;
    }
    return (
      <Item error={hasError} style={{ flexDirection: "column" }}>
        <Text style={{ alignSelf: "flex-start" }}>{label}</Text>
        <Input
          {...input}
          style={{
            borderColor: "steelblue",
            borderWidth: 1,
            margin: 5,
            height: 200,
            width: 360,
            padding: 5
          }}
          textAlignVertical="top"
          multiline={true}
        />
        {hasError ? (
          <Text style={{ alignSelf: "flex-end" }}>{error}</Text>
        ) : (
          <Text />
        )}
      </Item>
    );
  }
  async _openDatePicker() {
    try {
      console.log(this.state.selectedDate);
      const { action, year, month, day } = await DatePickerAndroid.open({
        date: new Date()
      });
      if (action !== DatePickerAndroid.dismissedAction) {
        this.setState({
          selectedDate: new Date(year, month, day) // month (0-11)
        });
      }
    } catch ({ code, message }) {
      console.warn("Can not open date picker", message);
    }
  }
  async _showImagePicker() {
    let result = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: false
    });
    if (!result.cancelled) {
      this.setState({
        fileUpload: result.uri
      });
    }
  }
  _renderDatePicker() {
    return (
      <Item>
        <Button
          transparent
          color={"steelblue"}
          onPress={() => this._openDatePicker()}
        >
          <Icon
            name="time"
            color="steelblue"
            style={{ height: 20, width: 20 }}
          />
        </Button>
        <Text>{moment(this.state.selectedDate).format("DD/MM")}</Text>
        {/* {hasError ? <Text>{error}</Text> : <Text />} */}
      </Item>
    );
  }
  _renderImagePicker() {
    return (
      <Item>
        <Button transparent small onPress={() => this._showImagePicker()}>
          <Icon
            name="image"
            color="mediumseagreen"
            style={{ height: 20, width: 20 }}
          />
        </Button>
      </Item>
    );
  }
  render() {
    const { hasSubmit, reset } = this.props;
    return (
      <View>
        <Field name="name" label={"Name: "} component={this._renderInput} />
        <Field name="phone" label={"Phone: "} component={this._renderInput} />
        <Field
          name="address"
          label={"Address: "}
          component={this._renderInput}
        />
        <Field name="date" component={this._renderDatePicker} />
        <Field
          name="content"
          label={"Content: "}
          component={this._renderContent}
        />
        <Field name="image" component={this._renderImagePicker} />
        {this.state.fileUpload !== null &&
          this.state.selectedDate <= new Date() && (
            <Button
              transparent
              color="mediumseagreen"
              large
              style={{ alignSelf: "flex-end" }}
            >
              <Icon name="send" color="steelblue" />
            </Button>
          )}
      </View>
    );
  }
}

const validate = values => {
  const err = {};
  err.address = "";
  var add = values.address;
  var ct = values.content;
  var img = values.image;
  if (values.address === undefined) {
    add = "";
  }
  if (values.content === undefined) {
    ct = "";
  }
  if (ct == "") {
    err.content = "Required";
  }
  if (add == "") {
    err.address = "Required";
  }
  return err;
};

const ReportForm = reduxForm({
  form: "simple",
  validate
})(SimpleForm);

export default ReportForm;
